import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { HttpModule} from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import {DataService} from './services/data.service';

import { AppComponent } from './app.component';
import { SandboxComponent } from './components/sandbox/sandbox.component';
import { Section4Component } from './components/section4/section4.component';
import { Section5Component } from './components/section5/section5.component';
import { Section5NgModuleFormComponent } from './components/section5-ng-module-form/section5-ng-module-form.component';
import { Section6Component } from './components/section6/section6.component';
import { HomeComponent } from './components/section7/home/home.component';
import { AboutComponent } from './components/section7/about/about.component';
import { NavbarComponent } from './components/section7/navbar/navbar.component';
import { UserDetailsComponent } from './components/section7/user-details/user-details.component';

const appRoutes: Routes = [
  {path:'',component:HomeComponent},
  {path:'about',component:AboutComponent},
  {path:'user/:id', component:UserDetailsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    SandboxComponent,
    Section4Component,
    Section5Component,
    Section5NgModuleFormComponent,
    Section6Component,
    HomeComponent,
    AboutComponent,
    NavbarComponent,
    UserDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    DataService
  ], 
  bootstrap: [AppComponent]
})
export class AppModule { }
