import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
  users:string[];
  data:Observable<Array<number>>;


  constructor(public http:Http) {
    this.users = ['Mark', 'Sharon', 'Beth'];
   }

   getUsers() {
     return this.users;
   }

   getUsersJsonPlaceholder() {
     return this.http.get('https://jsonplaceholder.typicode.com/users')
      .map(res => res.json());
   }
   
   getData() {
     this.data = new Observable(observer => {
       // Using arrow function here!
       setTimeout(() => {
        observer.next([1]);
       }, 5000);
       setTimeout(() => {
        observer.next([2]);
       }, 10000);
       setTimeout(() => {
        observer.next([4]);
       }, 15000);
       setTimeout(() => {
        observer.complete();
       }, 20000);
     });

     return this.data;
   }

   addUser(user) {
     return this.http.post('https://jsonplaceholder.typicode.com/users', user)
     .map(res => res.json());

   }

   deleteUser(id) {
    return this.http.delete('https://jsonplaceholder.typicode.com/users/'+id)
    .map(res => res.json());
  }

  updateUser(user) {
    return this.http.put('https://jsonplaceholder.typicode.com/users/'+user.id,user)
    .map(res => res.json());
  }

}
