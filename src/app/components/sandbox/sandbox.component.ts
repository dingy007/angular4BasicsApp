import { Component, OnInit } from '@angular/core';
import {Customer} from '../beans/customer'
@Component({
  selector: 'sandbox',
  templateUrl: './sandbox.component.html',
  styleUrls: ['./sandbox.component.css']
})
export class SandboxComponent implements OnInit {
  name:string = 'John Doe';
  age:number = 35;
  myNumberArray:number[] = [1,2,4];
  myAnyArray:any[] = [1,'x', 'hello'];
  myTuple:[string, number] = ['hello', 3, 'he', 4];
  myunusable: void = undefined;
  showName:boolean = false; // if else section 4 ch: 20
  greeting:number = 2;
  person:object = {
    firstName: 'Steve',
    lastName: 'Smith'
  };
  imageUrl:string = 'http://lorempixel.com/200/200';
  isUnchanged:boolean = false;

  customer:Customer;
  customers:Customer[];

  constructor() {
    console.log('Constructor initializing...');
    this.hasBirthday();
    this.customer = {
      name:'John Smith',
      id:1,
      email:'js@email.com'
    }
    this.customers = [
      {
        id:1,
        name:'John Smith',
        email: 'john@email.com'
      },
      {
        id:2,
        name:'Gary Long',
        email:'gary@email.com'
      }
    ]
   }
   hasBirthday() {
     this.age += 1;
   }
  ngOnInit() {
  }


  showAge() {
   return this.age;
  }
}