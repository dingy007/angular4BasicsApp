import { Component, OnInit } from '@angular/core';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-section6',
  templateUrl: './section6.component.html',
  styleUrls: ['./section6.component.css']
})
export class Section6Component implements OnInit {
  users:string[];
  data:any[] = [];
  usersJsonPlaceholder:any[];
  user = {
    id:'',
    name:'',
    email:'',
    phone:''
  }
  emptyUser = {id:'',name:'',email:'',phone:''};
  isEdit:boolean = false;

  constructor(public dataService:DataService) {
    console.log(this.dataService.getUsers());
    this.users = this.dataService.getUsers();

    this.dataService.getData().subscribe(
      data => {
        this.data.push(data);
      }
    );
    this.dataService.getUsersJsonPlaceholder().subscribe(users => {
      console.log(users);
      this.usersJsonPlaceholder = users;
    });
  }

  submit(isEdit) {
    console.log('Submitting to BE: isEdit: ' + isEdit);
    if (isEdit) {
      this.dataService.updateUser(this.user).subscribe(res => {
        for (let i=0; i<this.usersJsonPlaceholder.length; i++) {
          if (this.usersJsonPlaceholder[i].id == this.user.id) {
            console.log('Removing user: ' + this.user.name);
            this.usersJsonPlaceholder.splice(i,1);
          }
        }
        this.usersJsonPlaceholder.unshift(this.user);
        this.user = this.emptyUser;
      });
    } else {
      this.dataService.addUser(this.user).subscribe(user => {
        console.log(user);
        this.usersJsonPlaceholder.unshift(user);
        this.user = this.emptyUser;
      });
    }
  }

  onDeleteClick(id) {
    console.log('deleting userId: ' + id);
    this.dataService.deleteUser(id).subscribe(res => {
      console.log(res);
      for (let i =0; i<this.usersJsonPlaceholder.length;i++) {
        if (this.usersJsonPlaceholder[i].id == id) {
          console.log('Removing user: ' + this.usersJsonPlaceholder[i].name);
          this.usersJsonPlaceholder.splice(i,1);
        }
      }
    });
  }

  onEditClick(user) {
    console.log(user);
    this.isEdit = true;
    this.user = user;
  }
  ngOnInit() {
  }

}
