import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section5-ng-module-form',
  templateUrl: './section5-ng-module-form.component.html',
  styleUrls: ['./section5-ng-module-form.component.css']
})
export class Section5NgModuleFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  name:string ="";
  age:number=0;
  nameValueChanged(){
    console.log('nameValue: ' + this.name);
  }
  onSubmit() {
    console.log('Name value submitted: ' + this.name);
    console.log('Age value submitted: ' + this.age);
  }

  // Template driven forms
  user = {
    name:'',
    email:'',
    phone:''
  }

  onSubmitTemplate(form, valid) {
    console.log('Form submitted.');
    if (valid) {
      console.log(form);
    } else {
      console.log('Form is invalid');
    }
  }
}
