import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Section5NgModuleFormComponent } from './section5-ng-module-form.component';

describe('Section5NgModuleFormComponent', () => {
  let component: Section5NgModuleFormComponent;
  let fixture: ComponentFixture<Section5NgModuleFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Section5NgModuleFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Section5NgModuleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
