import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section4',
  templateUrl: './section4.component.html',
  styleUrls: ['./section4.component.css']
})
export class Section4Component {

  isSpecial1:boolean = true;
  canSave = false;
  currentStyle = {};

  setCurrrentStyle() {
    this.currentStyle = {
      'font-style':this.canSave ? 'italic' : 'normal',
      'font-size':this.isSpecial1 ? '24px' : '12px'
    }
  }

  // Pipes
  birthday = new Date(1981, 1, 15);

}
