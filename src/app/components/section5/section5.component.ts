import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
// import {Section5NgModuleFormComponent} from '../section5-ng-module-form/section5-ng-module-form.component';
// @NgModule({
//   declarations: [
//     Section5NgModuleFormComponent
//   ]
// })
@Component({
  selector: 'app-section5',
  templateUrl: './section5.component.html',
  styleUrls: ['./section5.component.css']
})
export class Section5Component implements OnInit {
  originalText:string = 'Hello world';
  text:string = this.originalText;
  clicked:boolean = false;
  keyboardEventType:string = 'keyboard event';
  constructor() { }

  ngOnInit() {
  }

  fireEvent(e, action) {
    console.log("Button action: " + e.type);
  } 
  changeValue(){
    this.clicked = !this.clicked;
    if (this.clicked) {
      this.text = 'goodbye world';
    } else {
      this.text = this.originalText;
    }
  }
  keyboardEvent(e) {
    console.log(e.type + ' event');
    this.keyboardEventType = e.type;
  }
}
